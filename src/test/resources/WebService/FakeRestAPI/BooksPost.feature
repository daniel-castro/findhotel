@Test
Feature: Post a book

  Scenario: Post a Book and validate status and response
    Given url 'http://fakerestapi.azurewebsites.net/api/Books'
    And header Content-Type = 'application/json'
    And header Accept = 'application/json'
    And request { ID: 1357, Title: 'The First Four Odd Numbers', Description: 'The ID numbers for dummies', PageCount: 1, Excerpt: 'What are the first four odd numbers?', PublishDate: '2019-03-11T05:16:13.247Z' }
    When method post
    Then status 200
    And match response == { ID: 1357, Title: 'The First Four Odd Numbers', Description: 'The ID numbers for dummies', PageCount: 1, Excerpt: 'What are the first four odd numbers?', PublishDate: '2019-03-11T05:16:13.247Z' }