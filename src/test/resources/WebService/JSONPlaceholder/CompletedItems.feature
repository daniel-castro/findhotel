@Test
Feature: Get completed items

  Scenario: Request Completed Items by GET
    Given url 'https://jsonplaceholder.typicode.com/users'
    #And param completed = 'true'
    When method get
    Then match $.*.name !contains ""
    And match $.*.username !contains ""
    And match $.*.email !contains ""
    And match $.*.email contains = '#regex[@]'