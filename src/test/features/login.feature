@Test
Feature: Sign In and Sign Out
  User should be able to access our website http://search.findhotel.net
  User should be able to be redirected to the Log In Page
  User should be able to perform the Sign In
  User should be able to perform the Sign Out

  Scenario: Sign In
    Given the user is in the page "https://search.findhotel.net/"
    And the user clicks on Log in
    When the user perform the Sign In with the username "daniel@kodeout.tk" and the password "12qwaszx!@"
    Then the "daniel@kodeout.tk" is shown in the nav bar of the "session"

  Scenario: Sign Out
    Given the user is in the page "https://search.findhotel.net/"
    When  the user perform the Sign Out
    Then the "Log in" is shown in the nav bar of the "home"
