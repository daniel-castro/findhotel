@Test
Feature: Localization
  Users should be able to access our website http://search.findhotel.net
  Users should be able to select their language
  Users should be able to select their currency

  Scenario: Change Language to English
    Given the user is in the page "https://search.findhotel.net/"
    When the user selects the language "English"
    Then the user should see the text "Log in" in the respective button

  Scenario: Change Currency to Brazilian Reais
    Given the user is in the page "https://search.findhotel.net/"
    When the user selects the currency "Brazil reais"
    Then the user should see the currency symbol