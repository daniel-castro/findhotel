@Test
Feature: Search Results
  User should be able to navigate between the result's pages
  User should be able to Show and Close the details on a Hotel Card

  Scenario: Navigate the Result's pages
    Given the user is in the page "https://search.findhotel.net/"
    When the user search for a hotel in "Amsterdam"
    And the results page is shown with a hotel card "Closed"
    Then navigate to the next results page should be possible

  Scenario: Show and Close the details on a Hotel Card
    Given the user is in the page "https://search.findhotel.net/"
    When the user search for a hotel in "Amsterdam"
    And the results page is shown with a hotel card "Closed"
    And clicking on the "Show details" button
    And the results page is shown with a hotel card "Open"
    And clicking on the "Close details" button
    Then the results page is shown with a hotel card "Closed"
