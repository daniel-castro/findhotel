@Test
Feature: Search Hotel
  User should be able to access our website http://search.findhotel.net
  User should be able to select a destination and dates to visit
  User should be able to be redirected to the search page
  User should be able to search for a specific hotel

  Scenario: Submit a search
    Given the user is in the page "https://search.findhotel.net/"
    When the user search for a hotel in "Amsterdam"
    Then the results page is shown with a hotel card "Closed"

  Scenario: Search for a specific Hotel
    Given the user is in the page "https://search.findhotel.net/"
    When the user search for the hotel "Holiday Inn Express Amsterdam - Sloterdijk Station"
    Then the user should see the specific result page