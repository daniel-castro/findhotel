package org.findHotel;

import PageFactory.HomePage;
import PageFactory.ResultsPage;
import PageFactory.LoginPage;
import PageFactory.SessionPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.lang.*;

public class StepDefinitions {

    public static WebDriver driver = new ChromeDriver();

    static WebDriverWait wait = new WebDriverWait(driver, 30);

    static HomePage objHome = new HomePage(driver);
    static ResultsPage objResults;
    static LoginPage objLogin;
    static SessionPage objSession;

    String currencyValue = "";
    String hotelName = "";

    @Given("^the user is in the page \"([^\"]*)\"$")
    public void userIsInThePage(String arg0) {
        objHome.getHomePage(driver, arg0);
    }

    @When("^the user selects the language \"([^\"]*)\"$")
    public void theUserSelectsTheLanguage(String arg0) {
        objHome.setLanguage(arg0);
    }

    @Then("^the user should see the text \"([^\"]*)\" in the respective button$")
    public void theUserShouldSeeTheTextInTheRespectiveButton(String arg0) {
        String loginButtonText = objHome.getLoginButton().getText();
        Assert.assertEquals(arg0, loginButtonText);
        objHome.closeCookies();
    }

    @When("^the user selects the currency \"([^\"]*)\"$")
    public void userSelectsTheCurrency(String arg0) {
        currencyValue = objHome.setCurrency(arg0);
    }

    @Then("^the user should see the currency symbol$")
    public void theUserShouldSeeTheCurrencySymbol() {
        Assert.assertEquals("R$", currencyValue);
    }

    @When("^the user search for a hotel in \"([^\"]*)\"$")
    public void theUserSearchForAHotelIn(String arg0) throws InterruptedException {
        objHome.setDestination(arg0, false);
        objHome.setStay();
        objResults = objHome.clickSearchButton(arg0);
    }

    @And("^the results page is shown with a hotel card \"([^\"]*)\"$")
    public void theResultsPageIsShown(String arg0) {
        objHome.closeCookies();
        if (arg0.equals("Open")) {
            Assert.assertTrue(objResults.checkResults("Close details"));
        } else if (arg0.equals("Closed")) {
            Assert.assertTrue(objResults.checkResults("Show details"));
        }
    }

    @Then("^navigate to the next results page should be possible$")
    public void navigateToTheNextResultsPage() {
        Assert.assertEquals("Previous", objResults.navNext());
    }

    @When("^the user search for the hotel \"([^\"]*)\"$")
    public void theUserSearchForTheHotel(String arg0) throws InterruptedException {
        hotelName = arg0;
        objHome.setDestination(hotelName, true);
        objHome.setStay();
        objResults = objHome.clickSearchButton(hotelName);
    }

    @Then("^the user should see the specific result page$")
    public void theUserShouldSeeTheSpecificResultPage() {
        System.out.println("- Waiting results with the hotel " + hotelName + "...");
        Assert.assertEquals(hotelName,objResults.getHotelName());

    }

    @And("^clicking on the \"([^\"]*)\" button$")
    public void clickingOnTheButton(String arg0) {
        System.out.println("- Clicking on the " + arg0 + " button...");
        String newDetailsButtonText = "";
        if(arg0.equals("Show details")){
            newDetailsButtonText = "Close details";
        } else if (arg0.equals("Close details")) {
            newDetailsButtonText = "Show details";
        }
        objHome.closeCookies();
        objResults.getDetailsButton(arg0).click();
        System.out.println("- Waiting results with " + newDetailsButtonText + " button...");
    }

    @And("^the user clicks on Log in$")
    public void theUserClicksOnLogIn() {
        objLogin = objHome.clickLoginButton();
    }

    @When("^the user perform the Sign In with the username \"([^\"]*)\" and the password \"([^\"]*)\"$")
    public void theUserPerformTheSignInWithTheUsernameAndThePassword(String arg0, String arg1)  {
        objSession = objLogin.performLogIn(arg0, arg1);
    }

    @Then("^the \"([^\"]*)\" is shown in the nav bar of the \"([^\"]*)\"$")
    public void theIsShownInTheNavBar(String arg0, String arg1)  {
        if (arg1.equals("session")){
            Assert.assertEquals(arg0, objSession.getUserWidgetName());
        } else if (arg1.equals("home")) {
            Assert.assertEquals("Log in", objHome.getLoginButton().getText());
        }
    }

    @When("^the user perform the Sign Out$")
    public void theUserPerformTheSignOut() {
        objSession.logOut();
    }
}