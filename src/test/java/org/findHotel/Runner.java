package org.findHotel;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:reports/cucumber-reports"},tags = "@Test", features = {"src/test/features"})
public class Runner {

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "c:/chromedriver_win32/chromedriver.exe");
    }

    @AfterClass
    public static void tearDownClass(){

        if (StepDefinitions.driver != null) {
            StepDefinitions.driver.quit();
        }
    }
}