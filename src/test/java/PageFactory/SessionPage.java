package PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SessionPage extends HomePage{

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath="//div[@data-id=\"UserWidgetName\"]")
    WebElement userWidgetName;

    @FindBy(xpath="//div[@data-id=\"UserWidget\"]/descendant::span[text()=\"Sign Out\"]")
    WebElement buttonSignOut;

    public SessionPage(WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    public String getUserWidgetName (){
        return userWidgetName.getText();
    }

    public void logOut(){
        wait.until(ExpectedConditions.elementToBeClickable(userWidgetName));
        userWidgetName.click();
        wait.until(ExpectedConditions.elementToBeClickable(buttonSignOut));
        buttonSignOut.click();
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
    }
}