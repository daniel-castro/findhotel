package PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.UtilitiesBelt;
import PageFactory.ResultsPage;


public class HomePage {

    WebDriver driver;
    WebDriverWait wait;
    static UtilitiesBelt tool = new UtilitiesBelt();
    static ResultsPage objResults;
    static LoginPage objLogin;
    static SessionPage objSession;

    @FindBy(xpath="//header/descendant::span[1]")
    WebElement loginButton;

    @FindBy(xpath="//header/descendant::span[2]")
    WebElement languageButton;

    @FindBy(xpath="//header/descendant::span[3]")
    WebElement currencyButton;

    @FindBy(xpath="//span[contains(text(),\"We use \")]/parent::div/following-sibling::a")
    WebElement closeCookiesButton;

    @FindBy(xpath="//input[@data-id=\"DestinationInput\"]")
    WebElement destinationInput;

    @FindBy(xpath="//li[@id=\"react-autowhatever-1-section-0-item-0\"]")
    WebElement destinationSuggestions;

    @FindBy(xpath="//li[contains(@id,\"react-autowhatever-1-section-\")]/descendant::span[text()=\"Property\"]")
    WebElement specificDestinationSuggestions;

    @FindBy(xpath="//div[@aria-disabled=\"false\" and @aria-selected=\"false\"][2]/div[@class=\"day-inner\"]\n")
    WebElement checkIn;

    @FindBy(xpath="//div[@aria-disabled=\"false\" and @aria-selected=\"false\"][4]/div[@class=\"day-inner\"]\n")
    WebElement checkOut;

    @FindBy(xpath="//button[@data-id=\"SearchButton\"]")
    WebElement searchButton;

    @FindBy(xpath="//button[@data-id=\"ShowDetailsButton\"]")
    WebElement showDetailsButton;

    public HomePage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    public void getHomePage(WebDriver driver, String url){
        System.out.println("\n### Scenario Started ###");
        System.out.println("- Loading the Home Page...");
        driver.get(url);

    }

    public WebElement getLoginButton(){
        return loginButton;
    }

    public void closeCookies(){
        boolean cookies = driver.findElements(By.xpath("//span[contains(text(),\"We use \")]/parent::div/following-sibling::a")).size() > 0;
        if (cookies) {
            System.out.println("- Closing Cookies notification.");
            wait.until(ExpectedConditions.elementToBeClickable(closeCookiesButton));
            closeCookiesButton.click();
        }
    }

    public void clickLanguageButton(){
        System.out.println("- Clicking the Language button...");
        wait.until(ExpectedConditions.elementToBeClickable(languageButton));
        languageButton.click();
    }

    public void setLanguage(String language){
        this.clickLanguageButton();
        System.out.println("- Changing the Language to "+ language + "...");
        driver.findElement(By.xpath("//span[text()=\"" + language + "\"]")).click();
    }

    public void clickCurrencyButton(){
        System.out.println("- Clicking the Currency button...");
        wait.until(ExpectedConditions.elementToBeClickable(currencyButton));
        currencyButton.click();
    }

    public String setCurrency(String newCurrency){
        this.clickCurrencyButton();
        WebElement newCurrencySymbol = driver.findElement(By.xpath("//span[text()=\"" + newCurrency + "\"]/preceding-sibling::span"));
        String newCurrencySymbolText = newCurrencySymbol.getText();
        System.out.println("- Changing the Currency to "+ newCurrency + "...");
        newCurrencySymbol.click();
        wait.until(ExpectedConditions.textToBePresentInElement(currencyButton,newCurrencySymbolText));
        System.out.println("- The Currency Symbol has changed to "+ newCurrencySymbolText + ".");
        return newCurrencySymbolText;
    }

    public void setDestination(String destination, Boolean specific){
        System.out.println("- Searching for a hotel in " + destination + "...");
        wait.until(ExpectedConditions.elementToBeClickable(destinationInput));
        tool.clearSendKeysField(destinationInput, destination);
        wait.until(ExpectedConditions.elementToBeClickable(destinationSuggestions));
        WebElement destinationSuggestion;
        if (specific) {
            destinationSuggestion = specificDestinationSuggestions;
        } else {
            destinationSuggestion = driver.findElement(By.xpath("//li[contains(@id,\"react-autowhatever-1-section-\")]/descendant::em[text()=\"" + destination + "\"]"));
        }
        destinationSuggestion.click();
        wait.until(ExpectedConditions.elementToBeClickable(checkIn));
    }

    public void setStay() throws InterruptedException {
        System.out.println("- Defining the stay period...");
        checkIn.click();
        checkOut.click();
    }

    public ResultsPage clickSearchButton(String destination){
        System.out.println("- Clicking Search button...");
        searchButton.click();
        wait.until(ExpectedConditions.titleContains(destination));
        objResults = new ResultsPage(driver);
        return objResults;
    }

    public LoginPage clickLoginButton(){
        System.out.println("- Clicking Log In button...");
        loginButton.click();
        wait.until(ExpectedConditions.titleContains("Sign In"));
        objLogin = new LoginPage(driver);
        return objLogin;
    }

}
