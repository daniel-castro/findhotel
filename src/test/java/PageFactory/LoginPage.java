package PageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends HomePage{

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath="//input[@name=\"email\"]")
    WebElement inputEmail;

    @FindBy(xpath="//input[@name=\"password\"]")
    WebElement inputPassword;

    @FindBy(xpath="//button[@name=\"submit\"]")
    WebElement buttonSubmit;

    @FindBy(xpath="//div[@data-id=\"UserWidgetName\"]")
    WebElement userWidgetName;

    public LoginPage(WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    public SessionPage clickButtonSubmit(){
        System.out.println("- Clicking Log In button...");
        buttonSubmit.click();
        wait.until(ExpectedConditions.elementToBeClickable(userWidgetName));
        objSession = new SessionPage(driver);
        return objSession;
    }

    public SessionPage performLogIn(String username, String password){
        wait.until(ExpectedConditions.elementToBeClickable(inputEmail));
        inputEmail.sendKeys(username);
        inputPassword.sendKeys(password);
        return objLogin.clickButtonSubmit();
    }
}