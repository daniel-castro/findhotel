package PageFactory;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.UtilitiesBelt;

public class ResultsPage extends HomePage{

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath="//button[@data-id=\"ShowDetailsButton\"]")
    WebElement showDetailsButton;

    @FindBy(xpath="//button[@data-id=\"CloseDetailsButton\"]")
    WebElement closeDetailsButton;

    @FindBy(xpath="//span[@data-id=\"HotelName\"]")
    WebElement hotelName;

    @FindBy(xpath="//button/descendant::span[text()=\"Previous\"]")
    WebElement previousButton;

    @FindBy(xpath="//button/descendant::span[text()=\"Next\"]")
    WebElement nextButton;

    public ResultsPage(WebDriver driver){
        super(driver);
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    public WebElement getDetailsButton(String detailsButtonText){
        WebElement detailsButton = null;
        if (detailsButtonText.equals("Show details")){
            wait.until(ExpectedConditions.elementToBeClickable(showDetailsButton));
            detailsButton = showDetailsButton;
        } else if (detailsButtonText.equals("Close details")) {
            wait.until(ExpectedConditions.elementToBeClickable(closeDetailsButton));
            detailsButton = closeDetailsButton;
        }
        return detailsButton;
    }

    public String getHotelName(){
        wait.until(ExpectedConditions.elementToBeClickable(hotelName));
        return hotelName.getText();
    }

    public String navNext(){
        System.out.println("- Navigating to the next result's page...");
        closeCookies();
        wait.until(ExpectedConditions.elementToBeClickable(nextButton));
        nextButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(previousButton));
        return previousButton.getText();
    }

    public boolean checkResults(String detailsButtonText){
        return getDetailsButton(detailsButtonText).getText().equals(detailsButtonText);
    }
}
